//
//  RectView.h
//  RectDemo
//
//  Created by Justin Andros on 11/10/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RectView : UIView

@property (nonatomic, strong) UIBezierPath *drawPath;
@property (nonatomic, assign) BOOL isAddingPoint;

- (IBAction)beginPointEntry:(id)sender;
- (IBAction)finishPointEntry:(id)sender;
- (IBAction)clearDrawPath:(id)sender;
- (IBAction)handleTap:(UIGestureRecognizer *)gr;

@end
