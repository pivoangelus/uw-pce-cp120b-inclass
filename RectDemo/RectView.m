//
//  RectView.m
//  RectDemo
//
//  Created by Justin Andros on 11/10/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "RectView.h"


@implementation RectView

- (void)drawBezierPath
{
    [self.drawPath fill];
    [self.drawPath stroke];
}

- (void)drawRect:(CGRect)rect
{
    NSLog(@"%@", NSStringFromCGRect(rect));
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.bounds];
    
    [[UIColor redColor] setFill];
    [path fill];
    
    if (self.isAddingPoint)
    {
        [[UIColor whiteColor] setFill];
    }
    else
    {
        [[UIColor blackColor] setFill];
    }
    [path fill];
    
    [[UIColor redColor] setStroke];
    [path stroke];
    
//    [[UIColor magentaColor] setFill];
//    [[UIColor colorWithWhite:.5 alpha:.8] setStroke];
    [self drawBezierPath];
}

- (UIBezierPath *)drawPath
{
    if (!_drawPath)
    {
        _drawPath = [UIBezierPath bezierPath];
    
        CGPoint pointA = { .x = 150, .y = 150. };
        CGPoint pointB = { .x = 100, .y = 200 };
        CGPoint pointC = { .x = 200, .y = 200 };
        
        [_drawPath moveToPoint:pointA];
        [_drawPath addLineToPoint:pointB];
        [_drawPath addLineToPoint:pointC];
        [_drawPath closePath];
    }
    return _drawPath;
}

- (IBAction)beginPointEntry:(id)sender;
{
    self.isAddingPoint = YES;
    [self setNeedsDisplay];
}

- (IBAction)finishPointEntry:(id)sender;
{
    self.isAddingPoint = NO;
    [self setNeedsDisplay];
}

- (IBAction)clearDrawPath:(id)sender;
{
    if (!self.isAddingPoint)
    {
        self.drawPath = [UIBezierPath bezierPath];
        [self setNeedsDisplay];
    }
}

- (IBAction)handleTap:(UIGestureRecognizer *)gr;
{
    NSLog(@"Tap detected: %@", gr);
    
    if (self.isAddingPoint)
    {
        CGPoint tapLocation = [gr locationInView:self];
    
        if (self.drawPath.isEmpty)
        {
            [self.drawPath moveToPoint:tapLocation];
        }
        else
        {
            [self.drawPath addLineToPoint:tapLocation];
        }
    }
    [self setNeedsDisplay];
}

@end
