//
//  Document.m
//  Camera Core Data
//
//  Created by Justin Andros on 11/30/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Document.h"
#import "Webcam.h"
#import "CameraGroup.h"
#import "CameraGroup+Human.h"

@interface Document ()

@end

@implementation Document

- (instancetype)init {
    self = [super init];
    if (self) {
        // Add your subclass-specific initialization here.
    }
    return self;
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController {
    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window.
}

+ (BOOL)autosavesInPlace {
    return YES;
}

- (NSString *)windowNibName {
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"Document";
}

- (IBAction)fetchIt:(id)sender
{
    // JSON datasource: http://mobileapp-wsdot.rhcloud.com/traveler/api/cameras
    NSError *error;
    NSData *rawData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://mobileapp-wsdot.rhcloud.com/traveler/api/cameras"]];
    
    if (!rawData)
        rawData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://faculty.washington.edu/hmueller/cameras.json"]];
    NSDictionary *completeDictionary = [NSJSONSerialization JSONObjectWithData:rawData
                                                                       options:kNilOptions
                                                                         error:&error];
    
    if (!completeDictionary)
        NSLog(@"%@ %@", error.localizedDescription, error.localizedFailureReason);
    
    [completeDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop)
    {
        //NSLog(@"%@ %@", key, obj);
    }];
    
    NSDictionary *allCamerasDictionary = completeDictionary[@"cameras"];
    //NSLog(@"last updated %@", allCamerasDictionary[@"updated"]);
    
    NSMutableDictionary *groupsDictionary = [NSMutableDictionary dictionary];
    for (NSDictionary *singleCameraDictionary in allCamerasDictionary[@"items"]) {
        //NSLog(@"%@", singleCameraDictionary);
        
        //NSManagedObject *group = groupsDictionary[singleCameraDictionary[@"roadName"]];
        CameraGroup *group = groupsDictionary[singleCameraDictionary[@"roadName"]];
        if (!group)
        {
            group = [CameraGroup findOrCreateGroupWithName:singleCameraDictionary[@"roadName"]
                                                   context:self.managedObjectContext];
            //above is the new method with Core Data written as code with Human method
            //  group = [NSEntityDescription
            //          insertNewObjectForEntityForName:@"CameraGroup"
            //          inManagedObjectContext:self.managedObjectContext];
            //  group.name = singleCameraDictionary[@"roadName"];
            //above is the new method with Core Data written as code
            //      [group setValue:singleCameraDictionary[@"roadName"] forKey:@"name"];
            //  groupsDictionary[singleCameraDictionary[@"roadName"]] = group;
        }
        
        //NSManagedObject *camera = [NSEntityDescription
        Webcam *camera = [NSEntityDescription
                          insertNewObjectForEntityForName:@"Webcam"
                          inManagedObjectContext:self.managedObjectContext];
        
        camera.id = singleCameraDictionary[@"id"];
        camera.latitude = singleCameraDictionary[@"lat"];
        camera.longitude = singleCameraDictionary[@"lon"];
        camera.title = singleCameraDictionary[@"title"];
        camera.url = singleCameraDictionary[@"url"];
        //above is the new method with Core Data written as code
        //  [camera setValue:singleCameraDictionary[@"id"] forKey:@"id"];
        //  [camera setValue:singleCameraDictionary[@"lat"] forKey:@"latitude"];
        //  [camera setValue:singleCameraDictionary[@"lon"] forKey:@"longitude"];
        //  [camera setValue:singleCameraDictionary[@"title"] forKey:@"title"];
        
        camera.group = group;
        //[camera setValue:group forKey:@"group"];
    }
    
}

@end
