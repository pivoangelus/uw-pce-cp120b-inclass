//
//  Document.h
//  Camera Core Data
//
//  Created by Justin Andros on 11/30/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface Document : NSPersistentDocument
@end
