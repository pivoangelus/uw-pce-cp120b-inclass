//
//  AppDelegate.m
//  Camera Core Data
//
//  Created by Justin Andros on 11/30/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
