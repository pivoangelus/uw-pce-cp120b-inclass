//
//  CameraGroup+Human.h
//  Camera Core Data
//
//  Created by Justin Andros on 11/30/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "CameraGroup.h"

@interface CameraGroup (Human)
+ (instancetype)findOrCreateGroupWithName:(NSString *)name context:(NSManagedObjectContext *)moc;
@end
