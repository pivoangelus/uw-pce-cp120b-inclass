//
//  CameraGroup+Human.m
//  Camera Core Data
//
//  Created by Justin Andros on 11/30/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "CameraGroup+Human.h"

@implementation CameraGroup (Human)

+ (instancetype)findOrCreateGroupWithName:(NSString *)name context:(NSManagedObjectContext *)moc
{
    NSDictionary *substitutionDictionary = @{@"nameToken": name};
    NSManagedObjectModel *model= moc.persistentStoreCoordinator.managedObjectModel;
    NSFetchRequest *fetchRequest = [model fetchRequestFromTemplateWithName:@"groupsWithName"
                                                     substitutionVariables:substitutionDictionary];
    NSError *error;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&error];
    if (!results)
    {
        NSLog(@"%@", error.localizedFailureReason);
        NSParameterAssert(NO);
    }
    else if (results.count == 1)
    {
        return [results firstObject];
    }

    CameraGroup *insertedResult = [self insertInManagedObjectContext:moc];
    insertedResult.name = name;
    return insertedResult;
}

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc
{
    CameraGroup *result = [NSEntityDescription
                           insertNewObjectForEntityForName:@"CameraGroup"
                                    inManagedObjectContext:moc];
    return result;
}

@end
