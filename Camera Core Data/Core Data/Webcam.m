//
//  Webcam.m
//  Camera Core Data
//
//  Created by Justin Andros on 11/30/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "Webcam.h"
#import "CameraGroup.h"

@implementation Webcam

@dynamic title;
@dynamic longitude;
@dynamic latitude;
@dynamic id;
@dynamic url;
@dynamic lastUpdated;
@dynamic isVideo;
@dynamic group;

@end
