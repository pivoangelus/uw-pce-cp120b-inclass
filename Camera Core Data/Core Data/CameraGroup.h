//
//  CameraGroup.h
//  Camera Core Data
//
//  Created by Justin Andros on 11/30/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Webcam;

@interface CameraGroup : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *cameras;
@end

@interface CameraGroup (CoreDataGeneratedAccessors)

- (void)addCamerasObject:(Webcam *)value;
- (void)removeCamerasObject:(Webcam *)value;
- (void)addCameras:(NSSet *)values;
- (void)removeCameras:(NSSet *)values;

@end
