//
//  ViewController.m
//  Networking
//
//  Created by Justin Andros on 12/7/14.
//  Copyright (c) 2014 Justin Andros. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *asyncDoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *syncDoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *backgroundDoneLabel;
@property (strong, nonatomic) NSMutableData *incomingData;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)performTask
{
    NSError *error;
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://wolframalpha.com/input/?i=1+1"] encoding:NSUTF8StringEncoding error:&error];
    NSLog(@"%@",result);
}

- (IBAction)performAsync:(id)sender
{
    self.asyncDoneLabel.text = @ "started";
    NSLog(@"async started");
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        [self performTask];
        dispatch_async(dispatch_get_main_queue(), ^{
           self.asyncDoneLabel.text = @"done";
        });
    });
}

- (IBAction)performSync:(id)sender
{
    self.syncDoneLabel.text = @"starting";
    NSLog(@"sync start");
    [self performTask];
    self.syncDoneLabel.text = @"3";
    self.syncDoneLabel.text = @"2";
    self.syncDoneLabel.text = @"done";
}

- (IBAction)performInBackground:(id)sender
{
    self.incomingData = [NSMutableData data];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://wolframalpha.com/input/?i=1+1"]];
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    
    NSLog(@"%@ started", connection);
    self.backgroundDoneLabel.text = @"started";
}

#pragma - mark delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"repsonse %@", response);
    self.backgroundDoneLabel.text = [NSString stringWithFormat:@"%@ %zd", response.textEncodingName, response.expectedContentLength];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.incomingData appendData:data];
    NSLog(@"%@", data);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"finished");
    self.backgroundDoneLabel.text = @"finished";
    NSLog(@"%@", self.incomingData);
    // !!!: assumes UTF8StringEncoding
    NSLog(@"%@", [[NSString alloc] initWithData:self.incomingData encoding:NSUTF8StringEncoding]);
}

@end
